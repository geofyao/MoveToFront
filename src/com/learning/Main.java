package com.learning;

import java.time.Instant;
import java.util.*;

public class Main {


    static Random random;

    public static void main(String[] args) {
        List<Integer> list=new ArrayList<>();
        List<Integer> sequence=new ArrayList<>();

        random= new Random(Instant.EPOCH.toEpochMilli());
        for(int i=0;i<100;i++){
            int k=random.nextInt(1000);
            while(list.contains(k))
                k=  random.nextInt(1000);
            list.add(k);
        }

        for(int i=0;i<10;i++){
            for(int j=0;j<list.size();j++){
                //sequence.add(random.nextInt(list.size()));
                sequence.add(list.get(j));
            }
        }
        Collections.shuffle(sequence);
        mf(new ArrayList<>(list),sequence);
        transpose(new ArrayList<>(list),sequence);
        frequencyCount(new ArrayList<>(list),sequence);

    }

    public static void mf(List<Integer> items, final List<Integer> sequence){
        int count= 0;
        List<Integer>list = new ArrayList<>();
        for(int z=0;z<items.size();z++){
            count+=2*list.size()+1;
            list.add(0,items.get(z));

        }
        System.out.println("MF did the insertions in : "+count+" steps!");

        for(int j=0;j<sequence.size();j++){
            /*int i=sequence.get(j);
            count+=sequence.get(j);*/
            int i=0;
           while (!Objects.equals(list.get(i), sequence.get(j))){
                count++;
                i++;
            }

            while(i!=0){
                count ++;
                int tmp=list.get(i-1);
                list.set(i-1,list.get(i));
                list.set(i,tmp);
                i--;
            }
        }

        System.out.println("MF did it in : "+count+" steps!");
    }

    public static void frequencyCount(List<Integer> items,final List<Integer> sequence){
        TreeMap<Integer,Integer> frequencies= new TreeMap<>();
        int count=0;
        List<Integer>list = new ArrayList<>();
        for(int z=0;z<items.size();z++){
            count+=list.size()+1;
            list.add(items.get(z));
            frequencies.putIfAbsent(items.get(z),1);
        }
        //on initialise le compteur
        for(int i=0;i<list.size();i++){
        }
        System.out.println("FC did the insertions in : "+count+" steps!");
        for(int j=0;j<sequence.size();j++){
            /*int i=sequence.get(j);
            count+=sequence.get(j);*/
            int i=0;
            while (!Objects.equals(list.get(i), sequence.get(j))){
                count++;
                i++;
            }
            //on incrémente la fréquence
            frequencies.put(list.get(i),frequencies.get(list.get(i))+1);
            boolean done =false;
            while(i>0 && !done){
                if(frequencies.get(list.get(i))> frequencies.get(list.get(i-1))){
                    count ++;
                    int tmp=list.get(i-1);
                    list.set(i-1,list.get(i));
                    list.set(i,tmp);
                    i--;
                }else {
                    done=true;
                }
            }
        }

        System.out.println("FC did it in : "+count+" steps!");

    }

    public static void transpose(List<Integer> items,final List<Integer> sequence){
        int count= 0;
        List<Integer>list = new ArrayList<>();
        for(int z=0;z<items.size();z++){
            if(list.size()==0){
                count+=list.size()+1;
                list.add(0,items.get(z));
            }else{
                count+=list.size()+2;
                list.add(list.size()-1,items.get(z));
            }

        }
        System.out.println("Transpose did the insertions in : "+count+" steps!");

        for(int j=0;j<sequence.size();j++){
            int i=0;
            while(!Objects.equals(list.get(i), sequence.get(j))){
                count++;
                i++;
            }

            if(i!=0){
                count ++;
                int tmp=list.get(i-1);
                list.set(i-1,list.get(i));
                list.set(i,tmp);
                i--;
            }
        }
        System.out.println("Transpose did it in : "+count+" steps!");

    }
}
